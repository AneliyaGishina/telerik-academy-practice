

import Pages.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class CreateNewTopic {
    private WebDriver webdriver;
    public String email = "kravdit@gmail.com";
    public String password = "buddyGroup3";
    public String tittle = "It's working";
    public String massage = "What is Lorem Ipsum?\\n\" +\n" +
            "                \"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

    @BeforeClass
    public static void classSetup(){
        WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();
    }

    @Before
    public void setup(){
        ChromeOptions options = new ChromeOptions();
//        options.setHeadless(true);
        webdriver = new ChromeDriver(options);

        webdriver.get("https://stage-forum.telerikacademy.com/");
        webdriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @After
    public void tearDown(){
        webdriver.quit();
    }

//  Using PageFactory
    @Test
    public void newTopic(){
        ForumIndexPage_PageFactory indexPage = new ForumIndexPage_PageFactory(webdriver);
        indexPage.pressLodInBtn();

        LogInPage_PageFactory logInPage = new LogInPage_PageFactory(webdriver);
        logInPage.AuthenticationWhitEmailAndPassword(email, password);

        ForumPage_PageFactory forumPage = new ForumPage_PageFactory(webdriver);
        forumPage.pressNewTopicButton();

        BlankNewTopicPage_PageFactory blankNewTopicPage = new BlankNewTopicPage_PageFactory(webdriver);
        blankNewTopicPage.writeNewTopic(tittle, massage);

        ViewTopicPage_PageFactory viewTopicPage = new ViewTopicPage_PageFactory(webdriver);
        Assert.assertTrue("New Topic is not created", viewTopicPage.topicEditButton.isDisplayed());
    }

    @Test
    public void createNewTopic(){
        ForumIndexPage indexPage = new ForumIndexPage(webdriver);
        indexPage.pressLodInBtn();

        LogInPage logInPage = new LogInPage(webdriver);
        logInPage.AuthenticationWhitEmailAndPassword(email, password);

        ForumPage forumPage = new ForumPage(webdriver);
        forumPage.pressNewTopicButton();

        BlankNewTopicPage blankNewTopicPage = new BlankNewTopicPage(webdriver);
        blankNewTopicPage.writeNewTopic(tittle, massage);

        ViewTopicPage viewTopicPage = new ViewTopicPage(webdriver);
        Assert.assertTrue("New Topic is not created", viewTopicPage.getTopicEditButton().isDisplayed());
    }
}


