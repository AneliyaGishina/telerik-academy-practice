package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BlankNewTopicPage {
    private WebDriver driver;

    public BlankNewTopicPage(WebDriver webDriver){
        driver = webDriver;
    }

    public WebElement getTittleTopic(){
        return driver.findElement(By.id("reply-title"));
    }

    public WebElement getMassageTopic(){
        return driver.findElement(By.tagName("textarea"));
    }

    public WebElement getCreateTopicButton(){
        return driver.findElement(By.xpath("//button[contains(.,'Create Topic')]"));
    }

    public void writeNewTopic(String tittle, String massage){
        getTittleTopic().sendKeys(tittle);
        getMassageTopic().sendKeys(massage);
        getCreateTopicButton().click();
    }
}
