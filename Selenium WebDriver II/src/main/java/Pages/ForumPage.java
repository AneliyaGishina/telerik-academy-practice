package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ForumPage {
    private WebDriver driver;

    public ForumPage(WebDriver webDriver){
        driver = webDriver;
    }

    public WebElement getAvatar(){
        return driver.findElement(By.xpath("//img[@tittle='kravdit']"));
    }

    public WebElement getNewTopicButton(){
        return driver.findElement(By.id("create-topic"));
    }

    public void pressNewTopicButton(){
        getNewTopicButton().click();
    }
}
