package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LogInPage {
    private WebDriver driver;

    public LogInPage(WebDriver webDriver){
        this.driver = webDriver;
    }

    public WebElement getEmailInput(){
        return driver.findElement(By.id("Email"));
    }

    public WebElement getPasswordInput(){
        return driver.findElement(By.id("Password"));
    }

    public WebElement getSingInButton(){
        return driver.findElement(By.id("next"));
    }

    public WebElement getLogo(){
        return driver.findElement(By.className("logo"));
    }

    public void AuthenticationWhitEmailAndPassword(String email, String password){
        getEmailInput().sendKeys(email);
        getPasswordInput().sendKeys(password);
        getSingInButton().click();
    }
}
