package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ViewTopicPage {
    private WebDriver driver;

    public ViewTopicPage(WebDriver webDriver){
        driver = webDriver;
    }

    public WebElement getTopicName(){
        return driver.findElement(By.xpath("//img[@tittle='kravdit']"));
    }

    public WebElement getTopicEditButton(){
        return driver.findElement(By.className("edit-topic"));
    }
}
