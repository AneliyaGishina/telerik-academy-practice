package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ForumPage_PageFactory {
    private WebDriver driver;

    public ForumPage_PageFactory(WebDriver webDriver){
        driver = webDriver;
        PageFactory.initElements(webDriver,this);
    }

    @FindBy(xpath = "//img[@tittle='kravdit']")
    public WebElement avatar;

    @FindBy(id = "create-topic")
    public WebElement newTopicButton;

    public void pressNewTopicButton(){
        newTopicButton.click();
    }
}
