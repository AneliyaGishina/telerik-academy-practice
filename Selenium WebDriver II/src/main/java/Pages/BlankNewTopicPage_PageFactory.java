package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BlankNewTopicPage_PageFactory {
    private WebDriver driver;

    public BlankNewTopicPage_PageFactory(WebDriver webDriver){
        driver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "reply-title")
    public WebElement tittleTopic;

    @FindBy(tagName = "textarea")
    public WebElement massageTopic;

    @FindBy(xpath = "//button[contains(.,'Create Topic')]")
    public WebElement createTopicButton;

    public void writeNewTopic(String tittle, String massage){
        tittleTopic.sendKeys(tittle);
        massageTopic.sendKeys(massage);
        createTopicButton.click();
    }
}
