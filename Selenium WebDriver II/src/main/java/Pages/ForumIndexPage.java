package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ForumIndexPage {
    private WebDriver driver;
    public ForumIndexPage(WebDriver webDriver){
        this.driver = webDriver;
    }

    public WebElement getLogInButton(){
        return driver.findElement(By.xpath("//button[contains(.,'Log In')]"));
    }

    public WebElement getLogo(){
        return driver.findElement(By.id("site-logo"));
    }

    public void pressLodInBtn(){
        getLogInButton().click();
    }

}
