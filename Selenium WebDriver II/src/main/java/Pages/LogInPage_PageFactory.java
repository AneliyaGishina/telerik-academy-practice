package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.page.Page;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogInPage_PageFactory {
    private WebDriver driver;

    public LogInPage_PageFactory(WebDriver webDriver){
        driver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id="Email")
    public WebElement emailInput;

    @FindBy(id="Password")
    public WebElement passwordInput;

    @FindBy(id="next")
    public WebElement singInButton;

    @FindBy(className = "logo")
    public WebElement logo;

    public void AuthenticationWhitEmailAndPassword(String email, String password){
        emailInput.sendKeys(email);
        passwordInput.sendKeys(password);
        singInButton.click();
    }
}
