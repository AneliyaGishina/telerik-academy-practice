package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ViewTopicPage_PageFactory {
    private WebDriver driver;

    public ViewTopicPage_PageFactory(WebDriver webDriver){
        driver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//img[@tittle='kravdit']")
    public WebElement topicName;

    @FindBy(className = "edit-topic")
    public WebElement topicEditButton;
}
