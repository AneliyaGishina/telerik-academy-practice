package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ForumIndexPage_PageFactory {
    private WebDriver driver;

    public ForumIndexPage_PageFactory(WebDriver webDriver){
        driver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//button[contains(.,'Log In')]")
    public WebElement logInButton;

    @FindBy(id = "site-logo")
    public WebElement logo;

    public void pressLodInBtn(){
        logInButton.click();
    }
}
