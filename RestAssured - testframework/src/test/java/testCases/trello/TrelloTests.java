package testCases.trello;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pages.trello.BoardPage;
import pages.trello.BoardsPage;
import pages.trello.LoginPage;
import trelloapi.Models.BoardModel;
import trelloapi.TrelloAPI;

public class TrelloTests extends BaseTest{
    private BoardModel createdBoard;
    private TrelloAPI trelloAPI;

    @Before
    public void beforeTest(){
        trelloAPI = new TrelloAPI();
        trelloAPI.authenticate("user");
        createdBoard = trelloAPI.createBoard("Trello Board - " + System.currentTimeMillis(), true);
    }

    @Test
    public void openExistingBoard_When_BoardNameClicked() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("user");

        BoardsPage boardsPage = new BoardsPage(actions.getDriver());
        boardsPage.clickOnBoard(createdBoard.name);

        BoardPage boardPage = new BoardPage(actions.getDriver());
        boardPage.assertListExists("To Do");
    }

    @Test
    public void createNewCardInExistingBoard_When_CreateCardClicked() {

    }

    @Test
    public void moveCardBetweenStates_When_DragAndDropIsUsed() {
    }

    @Test
    public void DeleteBoard_When_DeleteButtonIsClicked() {
    }



    @After
    public void afterTest(){
        trelloAPI.deleteBoard(createdBoard);
    }
}
