package pages.trello;

import org.openqa.selenium.WebDriver;

public class BoardsPage extends BaseTrelloPage {

    public BoardsPage(WebDriver driver) {
        super(driver, "trello.boardsUrl");
    }

    public void clickOnBoard(String boardName){
        actions.waitForElementVisibleUntilTimeout("trello.boardsPage.boardByTeamAndName",30, boardName);
        actions.clickElement("trello.boardsPage.boardByTeamAndName", boardName);
    }
}
