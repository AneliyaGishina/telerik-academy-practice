package trelloapi.Models;

public class ListModel {
    public String id;
    public String name;
    public boolean closed;
    public int pos;
    public Object softLimit;
    public String idBoard;
    public boolean subscribed;
}
